<?php

namespace Drupal\agoratheme;

use Drupal\node\NodeInterface;

/**
 * Provides preprocessors and similar functions in context of the current theme.
 */
final class AgorathemeUtils {

  /**
   * Theme agnostic template_page_attachments_alter() implementation.
   */
  public static function pageAttachmentsAlter(array &$attachments) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    $is_error_page = in_array($route_name, [
      'system.401',
      'system.403',
      'system.404',
    ]);
    if ($is_error_page) {
      $attachments['#attached']['library'][] = self::getFullThemeLibraryKey('error-pages');
    }

    $is_user_page = in_array($route_name, [
      'user.login',
      'user.pass',
      'user.register',
    ]);
    if ($is_user_page) {
      $attachments['#attached']['library'][] = self::getFullThemeLibraryKey('user');
    }

    $is_node_page = $route_name === 'entity.node.canonical';
    if ($is_node_page) {
      $node = \Drupal::routeMatch()->getParameter('node');
      assert($node instanceof NodeInterface);
      if ($node->bundle() === 'contact') {
        $attachments['#attached']['library'][] = self::getFullThemeLibraryKey('pages-contact');
      }
    }
  }

  /**
   * Theme agnostic template_preprocess_menu_local_tasks() implementation.
   */
  public static function preprocessMenuLocalTasks(array &$variables) {
    $variables['#attached']['library'][] = self::getFullThemeLibraryKey('base-admin');
  }

  /**
   * Theme agnostic template_preprocess_status_messages() implementation.
   */
  public static function preprocessStatusMessages(array &$variables) {
    $variables['#attached']['library'][] = self::getFullThemeLibraryKey('aui-callout');
  }

  /**
   * Theme agnostic template_preprocess_banner_slider() implementation.
   */
  public static function preprocessBannerSlider(array &$variables) {
    if (count($variables['items']) > 1) {
      $variables['#attached']['library'][] = self::getFullThemeLibraryKey('swiper');
    }
  }

  /**
   * Load critical CSS library names.
   *
   * @return string[]
   *   An array of critical CSS library names.
   */
  public static function loadCriticalCssLibraryNames(): array {
    /** @var \Drupal\Core\Path\PathMatcherInterface $path_matcher */
    $path_matcher = \Drupal::service('path.matcher');
    /** @var \Drupal\Core\Asset\LibraryDependencyResolverInterface $library_dependency_resolver */
    $library_dependency_resolver = \Drupal::service('library.dependency_resolver');
    $critical_css_library_name = $path_matcher->isFrontPage() ? 'critical-front' : 'critical-default';
    $critical_css_library_name = self::getFullThemeLibraryKey($critical_css_library_name);
    return $library_dependency_resolver->getLibrariesWithDependencies([$critical_css_library_name]);
  }

  /**
   * Gets the critical CSS markup for the current page.
   *
   * @return string[]
   *   An array of critical CSS data. The "external" is an array containing
   *   external CSS urls (typically Typekit font stylesheet urls). The "inline"
   *   key contains the critical CSS markup for the current page, if available.
   */
  public static function getCriticalCss(): array {
    /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery */
    $library_discovery = \Drupal::service('library.discovery');

    $critical_css_libraries = self::loadCriticalCssLibraryNames();
    $external_css = [];
    $critical_css = '';
    foreach ($critical_css_libraries as $library_name) {
      [$extension, $name] = explode('/', $library_name);
      if ($library_info = $library_discovery->getLibraryByName($extension, $name)) {
        if (!empty($library_info['css'])) {
          foreach ($library_info['css'] as $css_info) {
            if ($css_info['type'] !== 'file') {
              if ($css_info['type'] === 'external') {
                $external_css[] = $css_info['data'];
              }
              continue;
            }
            $file_path = $css_info['data'];
            if (file_exists($file_path)) {
              $file_contents = file_get_contents($file_path);
              $critical_css .= $file_contents;
            }
          }
        }
      }
    }
    return [
      'external' => $external_css,
      'inline' => $critical_css,
    ];
  }

  /**
   * Returns the rendered hero for the current page (statically cached).
   *
   * @return array|null
   *   The hero image/video as render array or NULL, if not available.
   */
  public static function getHeroFromCurrentPage(): ?array {
    $hero = &drupal_static(__METHOD__);
    if (!isset($hero)) {
      $hero = [];
      $route_match = \Drupal::routeMatch();

      switch ($route_match->getRouteName()) {
        case 'entity.node.canonical':
          /** @var \Drupal\node\NodeInterface $node */
          $node = $route_match->getParameter('node');
          if ($node->hasField('field_hero') && !$node->get('field_hero')->isEmpty()) {
            $hero = $node->get('field_hero')->view('full');
          }
          elseif ($node->hasField('field_media_image') && !$node->get('field_media_image')->isEmpty()) {
            $hero = $node->get('field_media_image')->view('full');
          }
          break;
      }
    }

    return !empty($hero) ? $hero : NULL;
  }

  /**
   * Gets the active theme name.
   *
   * @return string
   *   The active theme name.
   */
  protected static function getActiveThemeName(): string {
    $active_theme_name = &drupal_static(__METHOD__);
    if (!isset($active_theme_name)) {
      /** @var \Drupal\Core\Theme\ThemeManagerInterface $theme_manager */
      $theme_manager = \Drupal::service('theme.manager');
      $active_theme_name = $theme_manager->getActiveTheme()->getName();
    }
    return $active_theme_name;
  }

  /**
   * Gets the fully qualified library name based on the current theme.
   *
   * @param string $library_name
   *   The library name (part after the slash).
   *
   * @return string
   *   The fully qualified library name.
   */
  protected static function getFullThemeLibraryKey(string $library_name): string {
    return sprintf('%s/%s', self::getActiveThemeName(), $library_name);
  }

}
