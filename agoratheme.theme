<?php

/**
 * @file
 * Functions to support theming in the agoratheme theme.
 */

use Drupal\Core\Locale\CountryManager;

/**
 * Implements template_preprocess_html().
 */
function agoratheme_preprocess_html(array &$variables) {
  // Get the contents of the SVG sprite.
  $svg_sprite = \Drupal::theme()->getActiveTheme()->getPath() . '/img/icons/icons.svg';
  if (file_exists($svg_sprite)) {
    $variables['svg_sprite'] = $svg_sprite;
    $icons = file_get_contents($svg_sprite);
    $icons = str_replace('<?xml version="1.0" encoding="utf-8"?>', '', $icons);
    $icons = trim($icons);

    // Add a new render array to page_bottom so the icons get added to the page.
    $variables['page_bottom']['icons'] = [
      '#type' => 'inline_template',
      '#template' => '<span style="display: none">' . $icons . '</span>',
    ];
  }

  $current_route_name = \Drupal::routeMatch()->getRouteName();
  if (in_array($current_route_name, ['user.login', 'user.pass', 'user.register'])) {
    $variables['attributes']['class'][] = 'page--user-auth';
  }
}

/**
 * Implements template_preprocess_page().
 */
function agoratheme_preprocess_page(array &$variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  $variables['is_401'] = $route_name == 'system.401';
  $variables['is_403'] = $route_name == 'system.403';
  $variables['is_404'] = $route_name == 'system.404';
  $variables['is_error_page'] = $variables['is_401'] || $variables['is_403'] || $variables['is_404'];

  if (!empty($variables['node'])) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $variables['node'];
    $node_type = $node->getType();
    $variables['attributes']['class'][] = 'page-type-' . $node_type;
  }
}

/**
 * Implements template_preprocess_field()
 */
function agoratheme_preprocess_field(array &$variables) {
  $identifier = sprintf('%s:%s', $variables['entity_type'], $variables['field_name']);

  switch ($identifier) {
    case 'node:locations':
      $variables['attributes']['class'][] = 'location-list';
      $variables['attributes']['class'][] = 'content-list';
      break;

    case 'location:phone':
      foreach ($variables['items'] as &$item) {
        $item['content']['#template'] = '<a href="tel:{{ value|replace({\' \': \'\', \'-\': \'\', \'/\': \'\', \'(0)\': \'\'}) }}">{{ value|nl2br }}</a>';
      }
      break;
  }
}

/**
 * Implements hook_preprocess_agoralocation_location().
 */
function agoratheme_preprocess_agoralocation_location(array &$variables) {
  foreach ($variables['items'] as &$item) {
    if (!empty($item['country_code']) && empty($item['country'])) {
      $item['country'] = CountryManager::getStandardList()[$item['country_code']];
    }
  }
}

/**
 * Implements template_preprocess_social_network().
 */
function agoratheme_preprocess_social_network(array &$variables) {
  $type = $variables['type'];
  $variables['name_formatted'] = $variables['name'];

  $fa_icon_mapping = agorasocial_fontawesome_mapping();

  $icon = $fa_icon_mapping[$type] ?? '';
  if (!empty($icon)) {
    $variables['icon'] = $icon;
    $variables['name_formatted'] = [
      '#type' => 'inline_template',
      '#template' => "{{ icon(icon_name) }}",
      '#context' => [
        'icon_name' => $icon,
      ],
    ];
  }
}

/**
 * Implements hook_preprocess_file_link().
 */
function agoratheme_preprocess_file_link(array &$variables) {
  // Check, if link is a string - this is the case on file uploads, at least
  // with webform 6.x ond Drupal 9.x, as it seems.
  if (is_string($variables['link'])) {
    return;
  }
  /** @var \Drupal\Core\Url $url */
  $url = $variables['link']['#url'];
  $attributes = $url->getOption('attributes');
  if (empty($attributes['target'])) {
    $attributes['target'] = '_blank';
  }
  if (empty($attributes['rel'])) {
    $attributes['rel'] = 'noopener';
  }
  $url->setOption('attributes', $attributes);
}
